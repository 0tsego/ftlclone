﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AtheneEntertainment.Utilities.Sprites;

public class SpriteAnimator : MonoBehaviour
{
    private SpriteRendererAnimator[] _animators = new SpriteRendererAnimator[4];

    public void AddAnimation(AnimationDirection animDir, Sprite[] frames)
    {
        var animatorIndex = (int)animDir;
        _animators[animatorIndex] = gameObject.AddComponent<SpriteRendererAnimator>();
        _animators[animatorIndex].SetData(frames, 16f);
    }

    public void AddAnimation(Sprite[] frames)
    {
        var animatorIndex = (int)AnimationDirection.Unknown;
        _animators[animatorIndex] = gameObject.AddComponent<SpriteRendererAnimator>();
        _animators[animatorIndex].SetData(frames, 16f);
    }

    public void Play(AnimationDirection animDir)
    {
        StopAllAnimators();
        var animatorIndex = (int)animDir;
        var animator = _animators[animatorIndex];
        if(animator != null)
            _animators[animatorIndex].Play();
    }

    public void Play()
    {
        Play(AnimationDirection.Unknown);
    }

    private void StopAllAnimators()
    {
        foreach (var animator in _animators)
        {
            if(animator != null)
                animator.Stop();
        }
    }

    public void Stop()
    {
        StopAllAnimators();
    }
}