﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController
{
    public GameObject shipRoot { get; private set; }

    private ObstaclesMap _obstacleMap;
    private MapData _mapData;
    private string _shipName;
    private DoorsManager _doorsManager;
    private RoomsManager _roomsManager;
    private RoomBorder _roomBorder;
    private List<UnitController> _units = new List<UnitController>();

    public ShipController(string name, MapData mapData)
    {
        _shipName = name;
        _mapData = mapData;
        _obstacleMap = new ObstaclesMap();
        _roomBorder = new RoomBorder(this);

        CreateShip();
    }

    private void CreateShip()
    {
        shipRoot = CreateRootGameObject();

        _doorsManager = new DoorsManager(_mapData, CONSTANTS.DOORS_TAG);
        var floorController = new LayerController(_mapData, CONSTANTS.FLOOR_TAG);
        var wallsController = new LayerController(_mapData, CONSTANTS.WALLS_TAG);

        var layerGameObject = _doorsManager.CreateLayer();
        layerGameObject.transform.SetParent(shipRoot.transform, false);

        floorController.CreateLayer().transform.SetParent(shipRoot.transform, false);
        wallsController.CreateLayer().transform.SetParent(shipRoot.transform, false);

        _roomsManager = new ShipScriber(_mapData).CreateRoomsManager();
    }

    private GameObject CreateRootGameObject()
    {
        var root = new GameObject();
        root.name = string.Format("Map: {0}", _shipName);

        return root;
    }

    public Vector2 CalculateMapCenterPosition()
    {
        if (_mapData == null)
            return Vector2.zero;

        var x = _mapData.WidthInPixels / 2;
        var y = -_mapData.HeightInPixels / 2;

        return new Vector2(x, y);
    }

    public Vector2[] FindPath(MatrixPositionInfo start, MatrixPositionInfo finish, MatrixPositionInfo[] additionalObstacles = null)
    {
        var tiles = _obstacleMap.ProduceMap(_mapData.layers);

        tiles[start.row, start.col].State = LeeStates.start;
        tiles[finish.row, finish.col].State = LeeStates.finish;

        if (additionalObstacles != null)
        {
            foreach(var obstaclePos in additionalObstacles)
            {
                tiles[obstaclePos.row, obstaclePos.col].State = LeeStates.block;
            }
        }

        foreach (var obstaclePos in _doorsManager.GetLockedDoorPositions())
        {
            tiles[obstaclePos.row, obstaclePos.col].State = LeeStates.block;
        }

        var pathFinder = new LeePathFinder();
        var path = pathFinder.CalculatePath(tiles);

        if (path == null || path.Length == 0)
        {
            Debug.Log("Cant find path");
            return null;
        }

        Array.Reverse(path);

        var waypoints = new Vector2[path.Length];
        for (var i = 0; i < waypoints.Length; i++)
            waypoints[i] = _mapData.GetTilePosition(path[i].row, path[i].col);

        return waypoints;
    }

    public MatrixPositionInfo ConvertWorldToMatrixPosition(Vector3 worldPosition)
    {
        var localPosition = worldPosition - shipRoot.transform.position;

        return ConvertLocalToMatrixPosition(localPosition);
    }

    public MatrixPositionInfo ConvertLocalToMatrixPosition(Vector3 localPosition)
    {
        var x = localPosition.x + _mapData.tilewidth / 2f;
        var y = -(localPosition.y - _mapData.tileheight / 2f);

        var i = (y / _mapData.tileheight);
        var j = (x / _mapData.tilewidth);

        if (i < 0 || j < 0 || i >= _mapData.height || j >= _mapData.width)
            return null;

        var matPos = new MatrixPositionInfo((int) i, (int) j);
        return matPos;
    }

    public Vector3 ConvertMatrixPositionToLocal(MatrixPositionInfo matrixPosition)
    {
        var x = matrixPosition.col * _mapData.tilewidth;
        var y = - matrixPosition.row * _mapData.tileheight;

        return new Vector3(x, y, 0f);
    }

    public Vector3 ConvertMatrixPositionToWorld(MatrixPositionInfo matrixPosition)
    {
        var localPosition = ConvertMatrixPositionToLocal(matrixPosition);
        var worldPosition = localPosition + shipRoot.transform.position;
        return worldPosition;
    }

    public void ShowRoomName(Vector3 worldPosition)
    {
        var matrixPosition = ConvertWorldToMatrixPosition(worldPosition);
        if (matrixPosition == null)
            return;

        var roomName = _roomsManager.GetRoomName(matrixPosition.row, matrixPosition.col);
        Debug.Log(roomName);
    }

    public void ShowRoomBorder(Vector3 worldPosition)
    {
        var matrixPosition = ConvertWorldToMatrixPosition(worldPosition);
        var matrixPositions = _roomsManager.GetRoomBoxBorder(matrixPosition.row, matrixPosition.col);
        _roomBorder.Create(matrixPositions);
        _roomBorder.Show();
    }

    public List<UnitController> GetAllUnitsInRoom(Vector3 worldPosition)
    {
        var mp = ConvertWorldToMatrixPosition(worldPosition);
        var r = _roomsManager.GetRoom(mp);
        var units = new List<UnitController>();

        for (var i = 0; i < _units.Count; i++)
        {
            var matrixPosition = ConvertWorldToMatrixPosition(_units[i].transform.position);
            var room = _roomsManager.GetRoom(matrixPosition);

            if (room != r)
                continue;

            units.Add(_units[i]);
        }

        return units;
    }
}