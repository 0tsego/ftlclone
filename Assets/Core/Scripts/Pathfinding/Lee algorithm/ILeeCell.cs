﻿public enum LeeStates { free, start, block, finish }

public interface ILeeCell
{
    LeeStates State { get; set; }
}