﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PrefabLoader
{
    private static Dictionary<string, GameObject> _collection = new Dictionary<string, GameObject>();

    /// <summary>
    /// At first, check saved collection. If prefab has not found, then load it from Resources
    /// </summary>
    /// <typeparam name="T">Type for GetComponent<>()</typeparam>
    /// <param name="prefabName">Path to Resources</param>
    /// <returns></returns>
    public static T GetPrefab<T>(string prefabName)
    {
        var prefab = TryGetPrefab<T>(prefabName, true);
        return prefab;
    }

    private static T TryGetPrefab<T>(string prefabName, bool saveToCollection)
    {
        var prefab = default(T);
        var prefabGameObject = LoadGameObjectFromResources(prefabName, saveToCollection);
        if (prefabGameObject != null)
            prefab = prefabGameObject.GetComponent<T>();

        return prefab;
    }

    public static GameObject LoadGameObjectFromResources(string prefabName, bool saveToCollection = true)
    {
        GameObject prefabGameObject = null;

        if (!_collection.TryGetValue(prefabName, out prefabGameObject))
        {
            prefabGameObject = Resources.Load(prefabName) as GameObject;
            if (saveToCollection && prefabGameObject != null)
                _collection.Add(prefabName, prefabGameObject);
        }

        return prefabGameObject;
    }
}