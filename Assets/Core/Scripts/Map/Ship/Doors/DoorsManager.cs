﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoorsManager: LayerController
{
    private List<DoorController> _controllers = new List<DoorController>();
    private List<MatrixPositionInfo> _positions = new List<MatrixPositionInfo>();

    public DoorsManager(MapData mapData, string layerName) : base(mapData, layerName) {}

    protected override GameObject CreateTile(LayerData layerData, int i, int j)
    {
        var tileGameObject = base.CreateTile(layerData, i, j);
        if (tileGameObject == null)
            return null;

        var controller = tileGameObject.AddComponent<DoorController>();
        _controllers.Add(controller);

        tileGameObject.AddComponent<BoxCollider2D>();

        _positions.Add(new MatrixPositionInfo(i, j));

        return tileGameObject;
    }

    public List<DoorController> GetLockedDoors()
    {
        var list = new List<DoorController>();

        foreach(var door in _controllers)
        {
            if (door.State == DoorState.locked)
                list.Add(door);
        }

        return list;
    }

    public List<MatrixPositionInfo> GetLockedDoorPositions()
    {
        var list = new List<MatrixPositionInfo>();

        for (var i = 0; i < _controllers.Count; i++)
        {
            if (_controllers[i].State == DoorState.locked)
                list.Add(_positions[i]);
        }

        return list;
    }
}