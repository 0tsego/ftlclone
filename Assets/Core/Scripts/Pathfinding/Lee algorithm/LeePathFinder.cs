﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeePathFinder
{
    public readonly MatrixPositionInfo [] vonNeumannNeighborhood;

    private int START_WAVE_VALUE = 1;
    private ILeeCell[,] _cells;
    private int[,] _waves;

    public LeePathFinder()
    {
        vonNeumannNeighborhood = new MatrixPositionInfo[4];
        vonNeumannNeighborhood[0] = new MatrixPositionInfo(0, -1);
        vonNeumannNeighborhood[1] = new MatrixPositionInfo(1,  0);
        vonNeumannNeighborhood[2] = new MatrixPositionInfo(0,  1);
        vonNeumannNeighborhood[3] = new MatrixPositionInfo(-1, 0);
    }

    public MatrixPositionInfo [] CalculatePath( ILeeCell [,] cells )
    {
        _cells = cells;
        _waves = new int[cells.GetLength(0), cells.GetLength(1)];

        if(!FindStart())
        {
            Debug.Log("Can't find start");
            return null;
        }

        var finishPosition = MakeWaves();
        if(finishPosition == null)
        {
            Debug.Log("Can't find finish");
            return null;
        }

        var path = FindPath(finishPosition);
        return path;
    }

    private bool FindStart()
    {
        for (var i = 0; i < _cells.GetLength(0); i++)
        {
            for (var j = 0; j < _cells.GetLength(1); j++)
            {
                if (_cells[i, j].State == LeeStates.start)
                {
                    _waves[i, j] = START_WAVE_VALUE;
                    return true;
                }
            }
        }

        return false;
    }

    private MatrixPositionInfo MakeWaves()
    {
        for (var waveNumber = 0; waveNumber < _cells.GetLength(0) * _cells.GetLength(1); waveNumber++)
        {
            for (var i = 0; i < _cells.GetLength(0); i++)
            {
                for (var j = 0; j < _cells.GetLength(1); j++)
                {
                    if (_waves[i, j] == 0)
                        continue;

                    for (var k = 0; k < vonNeumannNeighborhood.Length; k++)
                    {
                        var row = i + vonNeumannNeighborhood[k].row;
                        var col = j + vonNeumannNeighborhood[k].col;

                        if (row < 0 || row >= _cells.GetLength(0))
                            continue;

                        if (col < 0 || col >= _cells.GetLength(1))
                            continue;

                        switch (_cells[row, col].State)
                        {
                            case LeeStates.start:
                                break;
                            case LeeStates.block:
                                break;
                            case LeeStates.finish:
                                _waves[row, col] = _waves[i, j] + 2;
                                return new MatrixPositionInfo(row, col);
                            default: //free
                                if (_waves[row, col] == 0)
                                    _waves[row, col] = _waves[i, j] + 1;
                                break;
                        }
                    }
                }
            }
        }

        return null;
    }

    private MatrixPositionInfo[] FindPath(MatrixPositionInfo finishPos)
    {
        var minWave = int.MaxValue;
        var positions = new List<MatrixPositionInfo>();
        var position = new MatrixPositionInfo(finishPos);
        positions.Add(position);

        while (minWave != START_WAVE_VALUE)
        {
            for (var k = 0; k < vonNeumannNeighborhood.Length; k++)
            {
                var row = position.row + vonNeumannNeighborhood[k].row;
                var col = position.col + vonNeumannNeighborhood[k].col;

                if (row < 0 || row >= _cells.GetLength(0))
                    continue;

                if (col < 0 || col >= _cells.GetLength(1))
                    continue;

                if (_waves[row, col] > 0 && _waves[row, col] < minWave)
                {
                    minWave = _waves[row, col];
                    position = new MatrixPositionInfo(row, col);
                    break;
                }
            }

            positions.Add(position);
        }

        return positions.ToArray();
    }
}