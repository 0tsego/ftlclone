﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    protected UnitController unitController;

    public State(UnitController unitController)
    {
        this.unitController = unitController;
    }
}