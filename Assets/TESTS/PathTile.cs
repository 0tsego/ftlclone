﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathTile : MonoBehaviour, ILeeCell
{
    public LeeStates State { get; set; }
    private int mouseClickCount = (int)LeeStates.free;

    private void OnMouseDown()
    {
        if (++mouseClickCount > (int)LeeStates.finish)
            mouseClickCount = 0;

        SetState((LeeStates)mouseClickCount);
    }

    public void SetState(LeeStates state)
    {
        State = state;
        SetColor();
    }

    private void SetColor()
    {
        var renderer = GetComponent<SpriteRenderer>();

        switch (State)
        {
            case LeeStates.start:
                renderer.color = Color.green;
                break;
            case LeeStates.free:
                renderer.color = Color.white;
                break;
            case LeeStates.block:
                renderer.color = Color.black;
                break;
            case LeeStates.finish:
                renderer.color = Color.yellow;
                break;
        }
    }

    public void Highlight()
    {
        var renderer = GetComponent<SpriteRenderer>();
        renderer.color = Color.red;
    }
}