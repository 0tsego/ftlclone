﻿public static class CONSTANTS
{
    public const string FLOOR_TAG = "Floor";
    public const string DOORS_TAG = "Doors";
    public const string WALLS_TAG = "Walls";
    public const string LEVEL_BACKGROUND_IMAGE_TAG = "Background";
}