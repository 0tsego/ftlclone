﻿using UnityEngine;
using System;

[Serializable]
public class ObjectData
{
    public int gid;
    public int height;
    public int id;
    public string name;
    public int rotation;
    public string type;
    public bool visible;
    public int width;
    public float x;
    public float y;

    //for waypoints
    public Vector2[] polyline;
    public Vector2[] Waypoints
    {
        get
        {
            if (_waypoints == null)
            {
                _waypoints = new Vector2[polyline.Length];
                for (var i = 0; i < _waypoints.Length; i++)
                {
                    var wpX = polyline[i].x + x;
                    var wpY = polyline[i].y + y;
                    _waypoints[i] = new Vector2(wpX, -wpY);
                }
            }

            return _waypoints;
        }
    }

    private Vector2[] _waypoints;
}