﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MonsterData : EntityData
{
    private MonsterAnimationInfo _animationInfo;

    protected override AnimationInfo animationInfo
    {
        get
        {
            if (_animationInfo == null)
                _animationInfo = new MonsterAnimationInfo(atlas, name);

            return _animationInfo;
        }
    }

    public Sprite[] GetWalkNorthAnim()
    {
        return GetWalkAnim(AnimationDirection.North);
    }

    public Sprite[] GetWalkSouthAnim()
    {
        return GetWalkAnim(AnimationDirection.South);
    }

    public Sprite[] GetWalkSideAnim()
    {
        return GetWalkAnim(AnimationDirection.Side);
    }

    private Sprite[] GetWalkAnim(AnimationDirection direction)
    {
        var animName = string.Format("{0}_{1}_{2}", name, AnimationType.Walk.ToString(), direction.ToString());
        return GetAnimation(animName);
    }
}
