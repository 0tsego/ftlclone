﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : State
{
    public Idle(UnitController unitController) : base(unitController)
    {
        unitController.StartCoroutine(Process());
    }

    private IEnumerator Process()
    {
        while (true)
        {
            var point = unitController.GetNextPathPoint();
            if (point.z < 0)
            {
                yield return new WaitForSeconds(0.2f);
                continue;
            }

            var newState = new Move(unitController, point);
            unitController.SetNewState(newState);
            break;
        }
    }
}