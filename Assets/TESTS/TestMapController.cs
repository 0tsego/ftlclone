﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMapController : MonoBehaviour
{
    public TextAsset shipMap;
    public GameObject waypointPrefab;
    public MatrixPositionInfo start;
    public MatrixPositionInfo finish;

    private ShipController _shipController;
    private UnitController _unitController;
    private List<GameObject> waypoints = new List<GameObject>();

    private void Start()
    {
        var mapData = JsonUtility.FromJson<MapData>(shipMap.text);
        _shipController = new ShipController(shipMap.name, mapData);
        _unitController = CreateUnit();

        start.row = 1;
        start.col = 0;

        finish.row = 5;
        finish.col = 4;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            var worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _shipController.ShowRoomBorder(worldPos);
            _unitController.Click(worldPos);
        }
    }

    public void FindPath()
    {
        Clear();

        var path = _shipController.FindPath(start, finish);
        if (path == null)
            return;

        foreach(var pos in path)
        {
            var wp = Instantiate(waypointPrefab);
            wp.transform.position = pos;
            waypoints.Add(wp);
        }
    }

    public void Clear()
    {
        foreach (var go in waypoints)
            Destroy(go);

        waypoints.Clear();
    }

    private UnitController CreateUnit()
    {
        var prefab = Resources.Load<UnitController>("Unit");
        var unit = Instantiate(prefab) as UnitController;
        unit.Initialize(_shipController);
        unit.transform.position = new Vector3(0f, -16f, 0f);

        return unit;
    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(30, 30, 60, 40), "FIND"))
            FindPath();

        if (GUI.Button(new Rect(30, 80, 60, 40), "clear"))
            Clear();
    }
}