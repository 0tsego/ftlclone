﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LayerController
{
    private MapData _mapData;
    private string _layerName;
    private TileCreator _tileCreator = new TileCreator();

    public LayerController(MapData mapData, string layerName)
    {
        _mapData = mapData;
        _layerName = layerName;
    }

    public virtual GameObject CreateLayer()
    {
        var layerData = Array.Find(_mapData.layers, (l) => { return l.name == _layerName; });
        var layerGameObject = CreateLayerTiles(layerData);
        if(layerGameObject != null)
            layerGameObject.name = string.Format("Layer: {0}", layerData.name);

        return layerGameObject;
    }

    private GameObject CreateLayerTiles(LayerData layerData)
    {
        if (layerData.Structure == null)
            return null;

        var layerGameObject = new GameObject();

        for (var i = 0; i < layerData.Structure.GetLength(0); i++)
        {
            for (var j = 0; j < layerData.Structure.GetLength(1); j++)
            {
                var tileGO = CreateTile(layerData, i, j);
                if (tileGO != null)
                    tileGO.transform.SetParent(layerGameObject.transform, false);
            }
        }

        return layerGameObject;
    }

    protected virtual GameObject CreateTile(LayerData layerData, int i, int j)
    {
        var gid = layerData.Structure[i, j];
        if (gid == 0)
            return null;

        var tilesetData = TiledUtilities.FindTilesetData(_mapData, gid);
        var tileGameObject = _tileCreator.CreateTile(tilesetData, gid, layerData.name);

        tileGameObject.transform.position = _mapData.GetTilePosition(i, j);
        tileGameObject.name = string.Format("Tile: gid = {0}, pos = ({1},{2})", gid, i, j);

        return tileGameObject;
    }
}