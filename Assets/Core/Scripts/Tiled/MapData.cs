﻿using UnityEngine;
using System;

[Serializable]
public class MapData
{
    public int height;
    public int width;
    public int tileheight;
    public int tilewidth;
    public LayerData[] layers;
    public TilesetData[] tilesets;

    public int HeightInPixels
    {
        get
        {
            return height * tileheight;
        }
    }

    public int WidthInPixels
    {
        get
        {
            return width * tilewidth;
        }
    }

    public Vector3 GetTilePosition(int row, int col)
    {
        var x = col * tilewidth;
        var y = -row * tileheight;
        var tilePosition = new Vector3(x, y, 0f);
        return tilePosition;
    }
}