﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsManager
{
    private List<Room> _rooms = new List<Room>();

    public RoomsManager()
    {

    }

    public void Add(Room room)
    {
        room.CalculateBoxCorners();
        _rooms.Add(room);
    }

    public void Add(Room[] rooms)
    {
        foreach (var room in rooms)
            Add(room);
    }

    public MatrixPositionInfo[] GetRoomBoxBorder(int i, int j)
    {
        var room = GetRoom(i, j);
        if (room == null)
            return null;

        return room.boxCorners;
    }

    public Room GetRoom(int i, int j)
    {
        var room = _rooms.Find((r) => { return r.Contains(i, j); });
        return room;
    }

    public Room GetRoom(MatrixPositionInfo matrixPosition)
    {
        var room = _rooms.Find((r) => { return r.Contains(matrixPosition.row, matrixPosition.col); });
        return room;
    }

    public string GetRoomName(int i, int j)
    {
        var room = GetRoom(i, j);

        if (room == null)
            return "";

        return room.Name;
    }
}