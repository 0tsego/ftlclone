﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum DoorState { locked, open }
public class DoorController : MonoBehaviour
{
    private DoorState _state;

    public DoorState State
    {
        get
        {
            return _state;
        }

        set
        {
            _state = value;

            switch (_state)
            {
                case DoorState.locked:
                    Close();
                    break;
                case DoorState.open:
                    Open();
                    break;
            }
        }
    }

    public void Open()
    {
        var renderer = GetComponent<SpriteRenderer>();
        renderer.enabled = false;
    }

    public void Close()
    {
        var renderer = GetComponent<SpriteRenderer>();
        renderer.enabled = true;
    }



    //FOR TEST PURPOSES:
    private int _mouseClickCounter = (int)DoorState.locked;

    private void OnMouseDown()
    {
        if (++_mouseClickCounter > (int)DoorState.open)
            _mouseClickCounter = 0;

        Debug.Log(_mouseClickCounter);
        State = (DoorState)_mouseClickCounter;
    }
}