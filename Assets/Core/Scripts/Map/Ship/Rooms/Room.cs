﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public string Name { get; private set; }
    public MatrixPositionInfo[] boxCorners = new MatrixPositionInfo[4];

    private List<MatrixPositionInfo> _scheme = new List<MatrixPositionInfo>();

    public Room(string name)
    {
        Name = name;
    }

    public void AddCell(int i, int j)
    {
        var matrixPos = new MatrixPositionInfo(i, j);
        _scheme.Add(matrixPos);
    }

    public bool Contains(int i, int j)
    {
        var matrixPos = _scheme.Find((pos) => { return pos.row == i && pos.col == j; });
        return matrixPos != null;
    }

    public void CalculateBoxCorners()
    {
        if (_scheme.Count == 0)
            return;

        var minRow = _scheme[0].row;
        foreach (var pos in _scheme)
            if (pos.row < minRow)
                minRow = pos.row;

        var minCol = _scheme[0].col;
        foreach (var pos in _scheme)
            if (pos.col < minCol)
                minCol = pos.col;

        var maxRow = _scheme[0].row;
        foreach (var pos in _scheme)
            if (pos.row > maxRow)
                maxRow = pos.row;

        var maxCol = _scheme[0].col;
        foreach (var pos in _scheme)
            if (pos.col > maxCol)
                maxCol = pos.col;

        boxCorners[0] = new MatrixPositionInfo(minRow, minCol);
        boxCorners[1] = new MatrixPositionInfo(minRow, maxCol);
        boxCorners[2] = new MatrixPositionInfo(maxRow, maxCol);
        boxCorners[3] = new MatrixPositionInfo(maxRow, minCol);
    }
}