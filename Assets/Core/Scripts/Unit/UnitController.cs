﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    private const float SPEED = 50f;

    private ShipController _shipController;
    private State _currentState;
    private Queue<Vector2> _path;

    public void Initialize(ShipController shipController)
    {
        _shipController = shipController;
        SetNewState(new Idle(this));
    }

    public void Click(Vector3 worldPosition)
    {
        CreatePath(worldPosition);
    }

    private void CreatePath(Vector3 finishWorldPosition)
    {
        var start = _shipController.ConvertWorldToMatrixPosition(transform.position);
        var finish = _shipController.ConvertWorldToMatrixPosition(finishWorldPosition);

        var path = _shipController.FindPath(start, finish);

        if (path != null && path.Length > 0)
            _path = new Queue<Vector2>(path);
    }

    public void SetNewState(State state)
    {
        _currentState = state;
    }

    public void MoveToPoint(Vector3 destination, Action onFinish)
    {
        StartCoroutine(Movement(destination, onFinish));
    }

    public Vector3 GetNextPathPoint()
    {
        if (_path == null || _path.Count == 0)
            return Vector3.back;

        var point = _path.Dequeue();

        return point;
    }

    private IEnumerator Movement(Vector3 destination, Action onFinish)
    {
        var startDirection = (destination - transform.position).normalized;
        var currentDirection = startDirection;

        if (currentDirection != Vector3.zero)
        {
            while (currentDirection == startDirection)
            {
                transform.position += startDirection * SPEED * Time.deltaTime;
                currentDirection = (destination - transform.position).normalized;

                yield return new WaitForEndOfFrame();
            }
        }

        transform.position = destination;
        if (onFinish != null)
            onFinish();
    }
}