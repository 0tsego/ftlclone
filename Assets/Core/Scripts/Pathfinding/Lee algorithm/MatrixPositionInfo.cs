﻿using System;

[Serializable]
public class MatrixPositionInfo
{
    public int row;
    public int col;

    public MatrixPositionInfo(int i, int j)
    {
        row = i;
        col = j;
    }

    public MatrixPositionInfo(MatrixPositionInfo info)
    {
        row = info.row;
        col = info.col;
    }

    public MatrixPositionInfo()
    {
        row = 0;
        col = 0;
    }

    public override string ToString()
    {
        var info = string.Format("({0},{1})", row, col);
        return info;
    }
}
