﻿using UnityEngine;

public class RoomBorder
{
    private ShipController _ship;
    private LineRenderer _border;
    private string _nameSuffix;

    public RoomBorder(ShipController ship, string name = "")
    {
        _ship = ship;
        _nameSuffix = name;
    }

    public void Create(MatrixPositionInfo[] roomCorners)
    {
        if (roomCorners == null || roomCorners.Length == 0)
            return;

        var points = CreateLinePoints(roomCorners);
        CreateNewGameObject(points);
    }

    public void Show()
    {
        if (_border == null)
            return;

        _border.gameObject.SetActive(true);
    }

    public void Hide()
    {
        if (_border == null)
            return;

        _border.gameObject.SetActive(false);
    }

    private void CreateNewGameObject(Vector3[] points)
    {
        if (_border != null)
            Object.Destroy(_border.gameObject);

        var line = new GameObject();
        line.SetActive(false);
        line.name = Name;
        line.transform.position = new Vector3(0f, 0f, -1f);
        line.transform.SetParent(_ship.shipRoot.transform);
        _border = line.AddComponent<LineRenderer>();
        _border.widthMultiplier = 5f;
        _border.useWorldSpace = false;
        _border.positionCount = points.Length;
        _border.SetPositions(points);
        //_border.sortingLayerName = "Overall";
    }

    private Vector3[] CreateLinePoints(MatrixPositionInfo[] roomCorners)
    {
        var pointsLength = roomCorners.Length + 1;
        var points = new Vector3[pointsLength];

        for(var i = 0; i < roomCorners.Length; i++)
            points[i] = _ship.ConvertMatrixPositionToWorld(roomCorners[i]);

        points[pointsLength - 1] = points[0];
        return points;
    }

    public string Name
    {
        get
        {
            var name = "Border";
            if (!string.IsNullOrEmpty(_nameSuffix))
                name += (": " + _nameSuffix);

            return name;
        }
    }
}