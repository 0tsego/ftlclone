﻿using UnityEngine;

public class TileCreator
{
    private TilesetData _tilesetData;
    private int _gid;
    private GameObject _tilePrefab;

    public TileCreator()
    {
        _tilePrefab = Resources.Load<GameObject>("Tile");
    }

    public GameObject CreateTile(TilesetData tilesetData, int gid, string sortingLayerName = "Default", int sortingOrder = 0)
    {
        if (gid == 0)
            return null;

        _tilesetData = tilesetData;
        _gid = gid;

        var tileSprite = Sprite.Create(tilesetData.Texture, CalculateSpriteRect(), new Vector2(0.5f, 0.5f), 1f);
        var tileGameObject = Object.Instantiate(_tilePrefab) as GameObject;
        var spriteRenderer = tileGameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = tileSprite;
        SetSortingLayer(spriteRenderer, sortingLayerName);
        spriteRenderer.sortingOrder = sortingOrder;

        return tileGameObject;
    }

    private Rect CalculateSpriteRect()
    {
        var spriteIndexInTexture = _gid - _tilesetData.firstgid;
        var spriteRowIndex = spriteIndexInTexture / _tilesetData.columns;
        var spriteColIndex = spriteIndexInTexture - spriteRowIndex * _tilesetData.columns;
        var spriteX = spriteColIndex * _tilesetData.tilewidth;
        var spriteY = _tilesetData.imageheight - (spriteRowIndex + 1) * _tilesetData.tileheight;
        var spriteRect = new Rect(spriteX, spriteY, _tilesetData.tilewidth, _tilesetData.tileheight);

        return spriteRect;
    }

    private void SetSortingLayer(SpriteRenderer renderer, string sortingLayerName)
    {
        if (sortingLayerName == "Default")
            return;

        renderer.sortingLayerName = sortingLayerName;

        if (renderer.sortingLayerName == "Default")
            renderer.sortingLayerName = "Debug";
    }
}