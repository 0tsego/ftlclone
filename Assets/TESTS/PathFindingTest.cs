﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFindingTest : MonoBehaviour
{
    private const int FIELD_SIZE_1 = 20;
    private const int FIELD_SIZE_2 = 20;

    public PathTile pathTilePrefab;

    private PathTile[,] tiles;

    // Use this for initialization
    void Start()
    {
        tiles = new PathTile[FIELD_SIZE_1, FIELD_SIZE_2];

        for(var i = 0; i < FIELD_SIZE_1; i ++)
        {
            for(var j = 0; j < FIELD_SIZE_2; j++)
            {
                var tile = Instantiate(pathTilePrefab);
                var pos = new Vector3(j * 10f, -i * 10f, 0f);
                tile.transform.position = pos;
                tiles[i, j] = tile;
            }
        }
    }

    private void FindPath()
    {
        var pathFinder = new LeePathFinder();
        var path = pathFinder.CalculatePath(tiles);

        foreach (var pos in path)
            tiles[pos.row, pos.col].Highlight();
    }

    private void Clear()
    {
        for (var i = 0; i < FIELD_SIZE_1; i++)
        {
            for (var j = 0; j < FIELD_SIZE_2; j++)
            {
                tiles[i, j].SetState(LeeStates.free);
            }
        }
    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(30, 30, 60, 40), "FIND"))
            FindPath();

        if (GUI.Button(new Rect(30, 80, 60, 40), "clear"))
            Clear();
    }
}