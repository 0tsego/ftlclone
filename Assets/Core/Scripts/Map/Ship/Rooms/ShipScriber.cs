﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShipScriber
{
    int[,] roomsScheme;
    private int currentRoomIndex = 2;

    public ShipScriber(MapData mapData)
    {
        var floorLayer = Array.Find(mapData.layers, (l) => { return l.name == CONSTANTS.FLOOR_TAG; });
        var wallsLayer = Array.Find(mapData.layers, (l) => { return l.name == CONSTANTS.WALLS_TAG; });
        var doorsLayer = Array.Find(mapData.layers, (l) => { return l.name == CONSTANTS.DOORS_TAG; });

        var h = floorLayer.Structure.GetLength(0);
        var w = floorLayer.Structure.GetLength(1);

        roomsScheme = new int[h, w];

        for (var i = 0; i < h; i++)
        {
            for (var j = 0; j < w; j++)
            {
                var isFree = floorLayer.Structure[i, j] > 0 && doorsLayer.Structure[i, j] == 0 && wallsLayer.Structure[i, j] == 0;
                roomsScheme[i, j] = (isFree ? 1 : 0);
            }
        }
    }

    public RoomsManager CreateRoomsManager()
    {
        for (var i = 0; i < roomsScheme.GetLength(0); i++)
        {
            for (var j = 0; j < roomsScheme.GetLength(1); j++)
            {
                if (roomsScheme[i, j] == 1)
                {
                    CutRoom(i, j);
                    currentRoomIndex++;
                }
            }
        }

        var roomCount = currentRoomIndex - 2;
        Debug.Log("roomCount = " + roomCount);
        var rooms = new Room[roomCount];

        for (var i = 0; i < roomsScheme.GetLength(0); i++)
        {
            for (var j = 0; j < roomsScheme.GetLength(1); j++)
            {
                if (roomsScheme[i, j] > 1)
                {
                    var roomIndex = roomsScheme[i, j] - 2;
                    if(rooms[roomIndex] == null)
                        rooms[roomIndex] = new Room("Room #" + roomIndex);

                    rooms[roomIndex].AddCell(i, j);
                }
            }
        }

        var roomsManager = new RoomsManager();
        roomsManager.Add(rooms);

        return roomsManager;
    }

    private void CutRoom(int i, int j)
    {
        if (i < 0 || j < 0 || i >= roomsScheme.GetLength(0) || j >= roomsScheme.GetLength(1))
            return;

        if (roomsScheme[i, j] == 0 || roomsScheme[i, j] == currentRoomIndex)
            return;

        roomsScheme[i, j] = currentRoomIndex;

        CutRoom(i + 1, j);
        CutRoom(i - 1, j);
        CutRoom(i, j + 1);
        CutRoom(i, j - 1);
    }


}