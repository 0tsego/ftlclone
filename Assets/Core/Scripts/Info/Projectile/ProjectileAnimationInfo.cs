﻿public class ProjectileAnimationInfo : AnimationInfo
{
    public ProjectileAnimationInfo(string atlasName, string targetName) : base(atlasName, targetName)
    {
        TryToAddMainAnimation();
        TryToAddExplosionAnimation();
    }

    protected bool TryToAddMainAnimation()
    {
        var animName = string.Format("{0}_Main", _targetName);
        return TryToAddAnimation(animName);
    }

    protected bool TryToAddExplosionAnimation()
    {
        var animName = string.Format("{0}_Explosion", _targetName);
        return TryToAddAnimation(animName);
    }
}