﻿using UnityEngine;

public class Move : State
{
    public Move(UnitController unitController, Vector3 destination) : base(unitController)
    {
        unitController.MoveToPoint(destination, OnFinish);
    }

    private void OnFinish()
    {
        unitController.SetNewState(new Idle(unitController));
    }
}