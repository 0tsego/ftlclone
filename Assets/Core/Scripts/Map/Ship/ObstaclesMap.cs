﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObstaclesMap
{
    private TileInfo[,] _tiles;

    public TileInfo[,] ProduceMap(LayerData[] layers)
    {
        if(layers == null || layers.Length == 0)
        {
            Debug.Log("There is no layers");
            return null;
        }

        InitializeTiles(layers[0].Structure.GetLength(0), layers[0].Structure.GetLength(1));

        foreach (var layer in layers)
            OperateLayer(layer);

        return _tiles;
    }

    private void InitializeTiles(int h, int w)
    {
        _tiles = new TileInfo[h, w];

        for (var i = 0; i < h; i++)
        {
            for (var j = 0; j < w; j++)
            {
                _tiles[i, j] = new TileInfo();
            }
        }
    }

    private void OperateLayer(LayerData layer)
    {
        switch (layer.name)
        {
            case "Floor":
                SetBlocksForSpecifiedGid(layer);
                break;
            case "Walls":
                SetBlocksForEntireLayer(layer);
                break;
        }
    }

    private void SetBlocksForSpecifiedGid(LayerData layer, int gid = 0)
    {
        for (var i = 0; i < _tiles.GetLength(0); i++)
        {
            for (var j = 0; j < _tiles.GetLength(1); j++)
            {
                if (layer.Structure[i, j] == gid)
                    _tiles[i, j].State = LeeStates.block;
            }
        }
    }

    private void SetBlocksForEntireLayer(LayerData layer)
    {
        for (var i = 0; i < _tiles.GetLength(0); i++)
        {
            for (var j = 0; j < _tiles.GetLength(1); j++)
            {
                if (layer.Structure[i, j] > 0)
                    _tiles[i, j].State = LeeStates.block;
            }
        }
    }
}