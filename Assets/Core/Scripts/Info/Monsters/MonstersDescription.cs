﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstersDescription
{
    public MonsterData[] content;

    public MonsterData this[string monsterName]
    {
        get
        {
            if (_content == null)
                FillDictionary();

            if (_content == null)
                return null;

            MonsterData monsterData = null;
            _content.TryGetValue(monsterName, out monsterData);

            return monsterData;
        }
    }

    private Dictionary<string, MonsterData> _content;
    private void FillDictionary()
    {
        if (content == null || content.Length == 0)
            return;

        _content = new Dictionary<string, MonsterData>();
        foreach (var monsterData in content)
            _content.Add(monsterData.name, monsterData);

        content = null;
    }
}